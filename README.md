# Sassy Bootstrap

**Sassy Bootstrap** is a bare bones Hugo theme integrating the Sass version of [Bootstrap 5](https://getbootstrap.com/)!

Apart from providing a reference for theme developers, one of the goals of Sassy Bootstrap is to make core Bootstrap [components](https://getbootstrap.com/docs/5.0/components) easy to access using [Hugo shortcodes](https://gohugo.io/templates/shortcode-templates/).

## How to use

1. **Install Hugo Extended**. Since Sassy Bootstrap uses the Sass version of Bootstrap, you need the extended version of Hugo. Full instructions 
   can be found at [gohugo.io](https://gohugo.io/getting-started/installing/).

2. **Clone this repository**
   ```git
   git clone git@gitlab.com:jwtay/sassy-bootstrap.git
   ```

3. **Serve pages using hugo server**
   ```
   cd sassy-bootstrap
   hugo server
   ```
  
   Navigate to `http://localhost:1313` (default)

3. **Edit the theme and site** and make it your own!

   * You should start by renaming the folder `themes\sassybootstrap` to `themes\<your-theme-name>`.
   * Update theme name in `config.toml` to `<your-theme-name>`.
   * Change the information in `themes\sassyboostrap\theme.toml` to your new theme's details. Please also add the following to properly attribute this theme:

   ```toml
   # If porting an existing theme
   [original]
       name = "Jian Wei Tay"
       homepage = "https://gitlab.com/jwtay/"
       repo = "https://gitlab.com/jwtay/sassy-bootstrap"
   ```
  
If you are new to theming in Hugo, I suggest reading the following:
* https://retrolog.io/blog/creating-a-hugo-theme-from-scratch/
* https://willschenk.com/articles/2018/building-a-hugo-site/

Keep in mind though that these are slightly outdated. Bootstrap is now at Version 5 and some things have changed. You might also want to consult the excellent [Bootstrap docs](https://getbootstrap.com/docs/5.0/getting-started/introduction/).

## Support

Sassy Bootstrap is very new and there may be bugs. If you encounter any, please [report them](https://gitlab.com/jwtay/sassy-bootstrap/-/issues).

## Roadmap

Shortcodes to implement these Bootstrap components:
- [ ] Accordion
- [x] Alerts
- [ ] Badge
- [ ] Breadcrumb
- [x] Buttons
- [ ] Button group
- [ ] Card - Underway!
- [ ] Carousel
- [ ] Close button
- [ ] Collapse
- [ ] Dropdowns
- [ ] List group
- [ ] Modal
- [ ] Navs & tabs
- [ ] Navbar
- [ ] Pagination
- [ ] Popovers
- [ ] Progress
- [ ] Scrollspy
- [ ] Spinners
- [ ] Toasts
- [ ] Tooltips

## Contributing

If you would like to contribute code, please submit a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html). Note that as the code is currently in active development, changes could happen at a rapid rate.

## Authors

* Jian Wei Tay

## License

This project is open source under the GNU General Public License V3 (GPL-3). This (very briefly) means that you may copy, distribute, and modify the software, as long as your code is made open source as well. See the [full license](https://gitlab.com/jwtay/sassy-bootstrap/-/blob/master/LICENSE) text for details.
