---
title: "Hello"
draft: false
---

# Sassy Bootstrap


## Buttons

{{< button theme="warning" value="Warning">}}
{{< button theme="danger" value="Alert">}}
{{< button value="Primary">}}

{{< button theme="danger" value="Alert" size="lg">}}
{{< button theme="danger" value="Alert" size="sm">}}

{{< button theme="danger" value="Hello!" state="disabled">}}

{{< button theme="info" href="#" value="home" state="enabled">}}
{{< button theme="info" href="#" value="home" state="disabled">}}

## Alerts

See more: [alerts](alerts)

{{< alert primary >}}
  This is a test
{{< /alert >}}

{{< alert secondary >}}
  This is a test
{{< /alert >}}

{{< alert success >}}
  This is a test
{{< /alert >}}

{{< alert danger >}}
  This is a test
{{< /alert >}}

{{< alert warning >}}
  This is a test
{{< /alert >}}

{{< alert info >}}
  This is a test
{{< /alert >}}

{{< alert light >}}
  This is a test
{{< /alert >}}

{{< alert dark >}}
  This is a test
{{< /alert >}}

## Cards

See [cards](/cards)