---
title: "Alerts"
draft: false
---

## Shortcodes

```go
{{</* alert primary */>}}
  This is a test
{{</* /alert */>}}

{{</* alert secondary */>}}
  This is a test
{{</* /alert */>}}

{{</* alert success */>}}
  This is a test
{{</* /alert */>}}

{{</* alert danger */>}}
  This is a test
{{</* /alert */>}}

{{</* alert warning */>}}
  This is a test
{{</* /alert */>}}

{{</* alert info */>}}
  This is a test
{{</* /alert */>}}

{{</* alert light */>}}
  This is a test
{{</* /alert */>}}

{{</* alert dark */>}}
  This is a test
{{</* /alert */>}}
```

Output:

{{< alert >}}
This is a test
{{< /alert >}}

{{< alert primary >}}
  This is a test
{{< /alert >}}

{{< alert secondary >}}
  This is a test
{{< /alert >}}

{{< alert success >}}
  This is a test
{{< /alert >}}

{{< alert danger >}}
  This is a test
{{< /alert >}}

{{< alert warning >}}
  This is a test
{{< /alert >}}

{{< alert info >}}
  This is a test
{{< /alert >}}

{{< alert light >}}
  This is a test
{{< /alert >}}

{{< alert dark >}}
  This is a test
{{< /alert >}}

{{< alert theme="dark" >}}
  This is a test
{{< /alert >}}