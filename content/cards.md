# Cards

## Syntax
```go
{{</* card width="100rem" title="This is a title" img="..." imgalt="..." */>}}
Some quick example text to build on the card title and make up the bulk of the card's content.
<a href="#" class="btn btn-primary">Go somewhere</a>
{{</* /card */>}}
```

{{< card width="100rem" title="This is a title" img="..." imgalt="..." >}}
Some quick example text to build on the card title and make up the bulk of the card's content.
<a href="#" class="btn btn-primary">Go somewhere</a>
{{< /card >}}